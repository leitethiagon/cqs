﻿using System;

namespace Cqs.Events
{
    public class CommandExecutedEvent : IEvent
    {
        public string CommandName { get; set; }
        public DateTime ExecutedOn { get; set; }
    }
}
