﻿using System;

namespace Cqs
{
    public interface IHandleEvent<TEvent>
        where TEvent : class, IEvent
    {
        void Handle(TEvent myevent);
    }
}
