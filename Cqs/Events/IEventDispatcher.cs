﻿using System;

namespace Cqs
{
    public interface IEventDispatcher
    {
        void Raise<TEvent>(TEvent myevent) where TEvent : class, IEvent;
    }
}
