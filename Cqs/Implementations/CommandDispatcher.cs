﻿using System;
using System.Linq;
using System.Collections.Concurrent;
using Cqs.Commands;

namespace Cqs.Implementations
{
    public sealed class CommandDispatcher : ICommandDispatcher
    {
        private readonly ConcurrentQueue<ICommand> CommandQueue = new ConcurrentQueue<ICommand>();
        private ICommandExecutor CommandExecutor;

        public CommandDispatcher(ICommandExecutor commandExecutor)
        {
            CommandExecutor = commandExecutor;
        }

        public void Dispatch<TCommand>(TCommand cmd) where TCommand : class, ICommand
        {
            CommandQueue.Enqueue(cmd);
        }

        public void StartExecuting()
        {
            if (CommandExecutor == null)
                throw new NullReferenceException("CommandExecutor");

            while (CommandQueue.Any())
            {
                ICommand cmd;
                if (CommandQueue.TryDequeue(out cmd))
                    CommandExecutor.Execute(cmd);
            }
        }
    }
}
