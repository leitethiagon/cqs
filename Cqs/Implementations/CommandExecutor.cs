﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Cqs.Commands;
using Cqs.Events;

namespace Cqs.Implementations
{
	public class CommandExecutor : ICommandExecutor 
    {
        private readonly IContainerResolver Container;
        private readonly IEventDispatcher EventDispatcher;

        public CommandExecutor(IContainerResolver container, IEventDispatcher eventDispatcher) 
        {
            Container = container;
            EventDispatcher = eventDispatcher;
		}

		#region ICommandExecutor Members

		public void Execute<TCommand>(TCommand cmd) where TCommand : class, ICommand 
        {
            var handlers = GetHandlers<TCommand>();

            foreach (var handler in handlers)
            {
                handler.Execute(cmd);

                EventDispatcher.Raise(new CommandExecutedEvent { CommandName = cmd.ToString(), ExecutedOn = DateTime.Now });

                handler.Dispose();
            }
		}

        public async Task ExecuteAsync<TCommand>(TCommand cmd) where TCommand : class, ICommand
        {
            var handlers = GetHandlers<TCommand>();

            await Task.Run(() => Parallel.ForEach(handlers, h => { 
                h.Execute(cmd); 
                h.Dispose(); 
            }));
        }

        private IEnumerable<IHandleCommand<TCommand>> GetHandlers<TCommand>() where TCommand : class, ICommand
        {
            var handlers = Container.ResolveAll<IHandleCommand<TCommand>>();

            if (!handlers.Any())
                throw new Exception(string.Format("There are no handlers for {0}", typeof(TCommand).ToString()));

            return handlers;
        }
		#endregion
	}
}
