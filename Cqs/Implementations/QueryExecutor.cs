﻿using System;
using System.Collections.Generic;
using Cqs.Queries;

namespace Cqs.Implementations
{
    public class QueryExecutor : IQueryExecutor
    {
        private readonly IEventDispatcher EventDispatcher;

        public QueryExecutor(IEventDispatcher eventDispatcher)
        {
            EventDispatcher = eventDispatcher;
        }

        public TResult Execute<TResult>(IQuery<TResult> query) 
            where TResult : class
        {
            return query.Execute();
        }
    }
}
