﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cqs.Implementations
{
    public class EventDispatcher : IEventDispatcher
    {
        private readonly IContainerResolver Container;

        public EventDispatcher(IContainerResolver container)
        {
            Container = container;
        }

        public void Raise<TEvent>(TEvent myevent) where TEvent : class, IEvent
        {
            var handlers = GetHandlers<TEvent>();

            foreach(var handler in handlers)
                handler.Handle(myevent);
        }

        private IEnumerable<IHandleEvent<TEvent>> GetHandlers<TEvent>() where TEvent : class, IEvent
        {
            var handlers = Container.ResolveAll<IHandleEvent<TEvent>>();

            if (!handlers.Any())
                throw new Exception(string.Format("There are no handlers for {0}", typeof(TEvent).ToString()));

            return handlers;
        }
    }
}
