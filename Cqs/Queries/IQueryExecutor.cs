﻿using System;

namespace Cqs.Queries
{
    public interface IQueryExecutor
    {
        TResult Execute<TResult>(IQuery<TResult> query) 
            where TResult : class;
    }
}
