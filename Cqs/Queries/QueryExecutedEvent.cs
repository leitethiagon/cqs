﻿using System;

namespace Cqs.Queries
{
    public class QueryExecutedEvent : IEvent
    {
        public string QueryName { get; set; }
    }
}