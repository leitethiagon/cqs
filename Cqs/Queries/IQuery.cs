﻿using System;

namespace Cqs.Queries
{
    public interface IQuery<TResult>
        where TResult : class
    {
        TResult Execute();
    }
}
