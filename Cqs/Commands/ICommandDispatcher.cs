﻿using System;

namespace Cqs.Commands
{
    public interface ICommandDispatcher
    {
        void Dispatch<TCommand>(TCommand cmd) where TCommand : class, ICommand;
    }
}
