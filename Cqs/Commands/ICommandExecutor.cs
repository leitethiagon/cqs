﻿using System;
using System.Threading.Tasks;

namespace Cqs.Commands
{
	public interface ICommandExecutor 
    {
		void Execute<TCommand>(TCommand cmd) where TCommand : class, ICommand;
        Task ExecuteAsync<TCommand>(TCommand cmd) where TCommand : class, ICommand;
	}
}
