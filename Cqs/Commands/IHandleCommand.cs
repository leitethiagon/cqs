﻿using System;

namespace Cqs.Commands
{
	public interface IHandleCommand<TCommand> : IDisposable 
        where TCommand : class, ICommand 
    {
		void Execute(TCommand cmd);
	}
}
