﻿using System;
using System.Collections.Generic;

namespace Cqs
{
    public interface IContainerResolver : IDisposable
    {
        void RegisterAll(System.Reflection.Assembly[] assemblies, Type ofType);
        void Register(Type componentType, Type ImplementationsType, Lifestyle lifeStyle = Lifestyle.Transient);
        void Register<TComponent, TImplementations>(Lifestyle lifestyle = Lifestyle.Transient);
        T Resolve<T>();
        IEnumerable<T> ResolveAll<T>();
    }
}
