﻿using System;

namespace Cqs
{
    public interface ILog
    {
        void Error(string message, Exception ex);
        void Trace(string message, Exception ex);
        void Debug(string message, Exception ex);
        void Warn(string message, Exception ex);
    }
}
