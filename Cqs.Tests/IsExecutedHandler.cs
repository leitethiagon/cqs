﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cqs.Commands;

namespace Cqs.Tests
{
	public class IsExecutedHandler : IHandleCommand<IsExecutedCommand> 
    {
        private ICommandExecutor Executor;

        public void Execute(IsExecutedCommand command)
        {
            command.IsExecuted = true;
		}

		public void Dispose() 
        {
		}
	}
}
