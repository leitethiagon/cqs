﻿using Cqs.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cqs.Tests.Queries
{
    public class EmptyQuery : IQuery<string>
    {
        public string Execute()
        {
            return "Hello world!";
        }
    }
}
