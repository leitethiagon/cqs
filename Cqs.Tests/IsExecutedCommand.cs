﻿using Cqs.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cqs.Tests
{
	public class IsExecutedCommand : ICommand 
    {
        public bool IsExecuted { get; set; }
	}
}
