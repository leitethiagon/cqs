﻿using System;
using System.Linq;
using Cqs.Implementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cqs;
using System.Collections.Generic;
using Moq;
using Cqs.Commands;

namespace Cqs.Tests {
	[TestClass]
	public class CommandExecutorTests 
    {
        [TestInitialize]
        public void setup()
        {
            
        }

		[TestMethod]
		public void can_execute_command() 
        {
            var command = new IsExecutedCommand();
            Assert.AreEqual(false, command.IsExecuted);

            var mock = new Mock<ICommandExecutor>();
            mock.Setup(m => m.Execute(command)).Verifiable("true");

            mock.Object.Execute(command);

            mock.Verify();
		}
	}
}
