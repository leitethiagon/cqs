﻿using Cqs.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cqs.Cmd
{
    public class CommandOneHandler : IHandleCommand<CommandOne>
    {
        private ICommandExecutor CommandExecutor;

        public CommandOneHandler(ICommandExecutor commandExecutor)
        {
            CommandExecutor = commandExecutor;
        }

        public void Execute(CommandOne cmd)
        {
            CommandExecutor.Execute(new CommandTwo());
        }

        public void Dispose()
        {
        }
    }

    public class CommandTwoHandler : IHandleCommand<CommandTwo>
    {
        public void Execute(CommandTwo cmd)
        {
        }

        public void Dispose()
        {
        }
    }
}
