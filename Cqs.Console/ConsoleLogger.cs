﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cqs.Cmd
{
    public class ConsoleLogger : ILog
    {
        public void Error(string message, Exception ex)
        {
            Console.WriteLine("Error: " + message);
        }

        public void Trace(string message, Exception ex)
        {
            Console.WriteLine("Trace: " + message);
        }

        public void Debug(string message, Exception ex)
        {
            Console.WriteLine("Debug: " + message);
        }

        public void Warn(string message, Exception ex)
        {
            Console.WriteLine("Warn: " + message);
        }
    }
}
