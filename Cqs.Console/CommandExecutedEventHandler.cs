﻿using Cqs.Events;

namespace Cqs.Infrastructure
{
    public class CommandExecutedEventHandler : IHandleEvent<CommandExecutedEvent>
    {
        private readonly ILog Logger;

        public CommandExecutedEventHandler(ILog logger)
        {
            Logger = logger;
        }

        public void Handle(CommandExecutedEvent myevent)
        {
            Logger.Trace("Command " + myevent.CommandName + " executed on " + myevent.ExecutedOn, null);
        }
    }
}
