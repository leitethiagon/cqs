﻿using Autofac;
using Autofac.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cqs.Cmd
{
    public class AutofacContainerResolver : IContainerResolver
    {
        private IContainer Container;

        public AutofacContainerResolver()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(this).As<IContainerResolver>().SingleInstance();

            Container = builder.Build();
        }

        public void RegisterAll(System.Reflection.Assembly[] assemblies, Type ofType)
        {
            var builder = new ContainerBuilder();

            builder.RegisterAssemblyTypes(assemblies)
                .AsClosedTypesOf(ofType);

            builder.Update(Container);
        }

        public void Register(Type componentType, Type ImplementationsType, Lifestyle lifeStyle = Lifestyle.Transient)
        {
            var builder = new ContainerBuilder();

            if (lifeStyle == Lifestyle.Singleton)
                builder.RegisterType(ImplementationsType).As(componentType).SingleInstance();
            else
                builder.RegisterType(ImplementationsType).As(componentType);

            builder.Update(Container);
        }

        public void Register<TComponent, TImplementations>(Lifestyle lifeStyle = Lifestyle.Transient)
        {
            var builder = new ContainerBuilder();

            if (lifeStyle == Lifestyle.Singleton)
                builder.RegisterType<TImplementations>().As<TComponent>().SingleInstance();
            else
                builder.RegisterType<TImplementations>().As<TComponent>();

            builder.Update(Container);
        }

        public T Resolve<T>()
        {
            return Container.Resolve<T>();
        }

        public IEnumerable<T> ResolveAll<T>()
        {
            return Container.Resolve<IEnumerable<T>>();
        }

        public void Dispose()
        {
            Container.Dispose();
        }
    }
}
