﻿using Autofac;
using Cqs.Commands;
using Cqs.Implementations;
using Cqs.Infrastructure;
using Cqs.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cqs.Cmd
{
    class Program
    {
        static void Main(string[] args)
        {
            var Container = new AutofacContainerResolver();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            Container.RegisterAll(assemblies, typeof(IHandleCommand<>));
            Container.RegisterAll(assemblies, typeof(IHandleEvent<>));
            Container.Register<ILog, ConsoleLogger>();
            Container.Register<IEventDispatcher, EventDispatcher>();
            Container.Register<ICommandExecutor, CommandExecutor>();
            Container.Register<IQueryExecutor, QueryExecutor>();

            var queryExecutor = Container.Resolve<IQueryExecutor>();
            var users = queryExecutor.Execute(new GetUsuariosQuery());

            var executor = Container.Resolve<ICommandExecutor>();
            executor.Execute(new CommandOne());
            executor.Execute(new CommandTwo { Test = "Valor" });

            Console.Read();
        }
    }
}
