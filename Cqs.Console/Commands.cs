﻿using Cqs.Commands;
using Cqs.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cqs.Cmd
{
    public class CommandOne : ICommand
    {
    }

    public class CommandTwo : ICommand
    {
        public string Test { get; set; }
    }

    public class GetUsuariosQuery : IQuery<IEnumerable<Usuario>>
    {
        public IEnumerable<Usuario> Execute()
        {
            return new List<Usuario>();
        }
    }

    public class Usuario
    {
    }
}
